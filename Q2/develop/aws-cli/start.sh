#!/usr/bin/env bash
#
# For unknown reason, cannot use service_name for aws_cli. 
# Set a env var MOTO_ENDPOINT for the ip
response=""
while [[ -z "${response}" ]]
do
    echo "wait 10 seconds ..."; sleep 10; 
    response=$(curl -vs http://moto_server:5000 2>&1 | grep "200")
done
moto_server_ip=$(curl -vs http://moto_server:5000 2>&1 | grep "Trying" | awk '{print $3}' | sed 's/\.\.\.//g')
echo "moto_server_ip=${moto_server_ip}"
export MOTO_ENDPOINT=http://${moto_server_ip}:5000
echo "MOTO_ENDPOINT=http://${moto_server_ip}:5000" > /root/.env
#
# Create a instance with name tag $EC2_NAME_TAG_KEY:$EC2_NAME_TAG_VALUE
aws ec2 run-instances --image-id ami-173d747e --count 1 --instance-type t1.micro --key-name MyKeyPair --security-groups my-sg --endpoint ${MOTO_ENDPOINT}
id=$(aws ec2 describe-instances --query Reservations[*].Instances[*].InstanceId --output text --endpoint ${MOTO_ENDPOINT})
aws ec2 create-tags --resources ${id} --tags Key=${EC2_NAME_TAG_KEY},Value=${EC2_NAME_TAG_VALUE} --endpoint ${MOTO_ENDPOINT}
#
while true; do printf "."; sleep 60; done
