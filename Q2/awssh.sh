#!/usr/bin/env sh
EC2_NAME_TAG_KEY="Name"
ip=$(aws ec2 describe-instances --filters "Name=tag:${EC2_NAME_TAG_KEY},Values=${1}" --query Reservations[0].Instances[0].PublicIpAddress --output text) 
if [[ ${ip} == "None" ]] 
then
    echo "Host not found"
    exit 1
fi
ssh ec2-user@${ip}
