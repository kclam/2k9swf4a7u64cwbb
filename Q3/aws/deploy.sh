#!/usr/bin/env sh
AWS_DIR=ecs
AWS_DEPLOY_DIR="$AWS_DIR/deploy"

# Prepare and check AWS deploy directory.
if [ ! -d "$AWS_DEPLOY_DIR" ]; then
  echo "error: directory for deployment, '$AWS_DIR', not exist."
  exit 1;
fi

apt-get install gettext-base

# Prepare holder directory for dynamic deployments.
rm -rf $AWS_DEPLOY_DIR/.generated && mkdir $AWS_DEPLOY_DIR/.generated

# Generate object configuration files for deployment.
for f in $AWS_DEPLOY_DIR/*.json
do
  envsubst < $f > "$AWS_DEPLOY_DIR/.generated/$(basename $f)"
done

# Deploy ECS.
cd "$AWS_DEPLOY_DIR/.generated"
aws ecs register-task-definition --cli-input-json "file://./task-definition.json" --region "${AWS_ECS_DEFAULT_REGION}"
aws ecs update-service --service ${AWS_ECS_PROJECT_NAME} --force-new-deployment --cli-input-json "file://./service.json" --region "${AWS_ECS_DEFAULT_REGION}" 
