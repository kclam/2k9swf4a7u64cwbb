package shortenurl

import (
	"log"
	"math/rand"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"

	"gitlab.com/kclam/2k9swf4a7u64cwbb/config"
)

// ShortenURL .
type ShortenURL struct {
	URL        string
	ShortenURL string
}

const (
	shortenURLLen = 9
	validChar     = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	collectionName = "shorten_urls"
)

var (
	svc *dynamodb.DynamoDB
)

func getRandomString() string {
	b := make([]byte, shortenURLLen)
	for i := range b {
		b[i] = validChar[rand.Intn(len(validChar))]
	}
	return string(b)
}

// Create .
func Create(url string) (string, error) {
	sURL := getRandomString()
	_, err := FindShortenURL(sURL)
	for err != nil {
		sURL = getRandomString()
		_, err = FindShortenURL(sURL)
	}

	s := ShortenURL{
		URL:        url,
		ShortenURL: sURL,
	}

	item, err := dynamodbattribute.MarshalMap(s)
	if err != nil {
		return "", err
	}

	configDB := config.Get().DB
	input := &dynamodb.PutItemInput{
		Item:      item,
		TableName: aws.String(configDB.DynamoTable),
	}

	_, err = svc.PutItem(input)
	if err != nil {
		return "", err
	}
	return sURL, nil
}

// FindURL .
func FindURL(url string) (*ShortenURL, error) {
	configDB := config.Get().DB
	filter := expression.Name("URL").Equal(expression.Value(url))
	proj := expression.NamesList(expression.Name("URL"), expression.Name("ShortenURL"))

	expr, err := expression.NewBuilder().WithFilter(filter).WithProjection(proj).Build()
	if err != nil {
		log.Printf("Got error building expression: %s", err.Error())
		return nil, err
	}

	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String(configDB.DynamoTable),
	}

	result, err := svc.Scan(params)
	if err != nil {
		log.Printf("Query API call failed: %s", err.Error())
		return nil, err
	}

	if len(result.Items) == 0 {
		return nil, nil
	}

	if len(result.Items) > 1 {
		log.Printf("duplicated items %+v", result.Items)
	}

	item := ShortenURL{}

	err = dynamodbattribute.UnmarshalMap(result.Items[0], &item)

	if err != nil {
		log.Printf("Got error unmarshalling: %s", err.Error())
		return nil, err
	}

	return &item, nil
}

// FindShortenURL .
func FindShortenURL(sURL string) (*ShortenURL, error) {
	configDB := config.Get().DB
	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(configDB.DynamoTable),
		Key: map[string]*dynamodb.AttributeValue{
			"ShortenURL": {
				S: aws.String(sURL),
			},
		},
	})
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	item := ShortenURL{}

	err = dynamodbattribute.UnmarshalMap(result.Item, &item)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return &item, nil

}

func createTable() error {
	configDB := config.Get().DB

	// Create table Movies
	tableName := configDB.DynamoTable

	input := &dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("ShortenURL"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("ShortenURL"),
				KeyType:       aws.String("HASH"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(10),
			WriteCapacityUnits: aws.Int64(10),
		},
		TableName: aws.String(tableName),
	}

	_, err := svc.CreateTable(input)
	if err != nil {
		log.Printf("%+v\n", err)
		return err
	}

	log.Printf("Successfully created table %s", tableName)
	return nil
}

func checkTable() error {
	_, err := FindShortenURL("check-table-dummy-request")
	if err != nil {
		if strings.Contains(err.Error(), dynamodb.ErrCodeResourceNotFoundException) {
			return createTable()
		}
		return err
	}
	return nil
}

// Init .
func Init() error {
	var err error

	rand.Seed(time.Now().UnixNano())

	configDB := config.Get().DB
	creds := credentials.NewStaticCredentials(configDB.DynamoAccessKeyID, configDB.DynamoSecretAccessKey, "")
	dSession, err := session.NewSession(
		&aws.Config{
			Credentials: creds,
			Region:      aws.String(configDB.DynamoRegion),
			Endpoint:    aws.String(configDB.DynamoAddress),
		},
	)
	if err != nil {
		return err
	}
	svc = dynamodb.New(dSession)

	return checkTable()
}
