module gitlab.com/kclam/2k9swf4a7u64cwbb

go 1.13

require (
	github.com/aws/aws-sdk-go v1.30.20
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/gorilla/mux v1.7.4
	github.com/prometheus/common v0.4.0
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/viper v1.6.3
	github.com/unrolled/render v1.0.3
	go.mongodb.org/mongo-driver v1.3.2
)
