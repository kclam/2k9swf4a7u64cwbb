package handler

import (
	"encoding/json"
	"log"
	"net/http"
	"regexp"

	"github.com/gorilla/mux"

	"gitlab.com/kclam/2k9swf4a7u64cwbb/httputil"
	"gitlab.com/kclam/2k9swf4a7u64cwbb/shortenurl"
)

var validShortenURL = regexp.MustCompile(`[a-zA-Z0-9]{9}$`)

// NewURL .
func NewURL(w http.ResponseWriter, r *http.Request) {

	params := struct {
		URL string `json:"url" valid:"required"`
	}{}

	err := json.NewDecoder(r.Body).Decode(&params)
	if err != nil {
		log.Printf("%s, Error: %s", "Invalid JSON", err)
		httputil.Error(w, http.StatusBadRequest, httputil.CodeErrInvalidJSON, "Invalid JSON")
		return
	}

	type Response struct {
		URL        string `json:"url"`
		ShortenURL string `json:"shortenUrl"`
	}

	s, err := shortenurl.FindURL(params.URL)
	if err != nil {
		log.Printf("%s, Warning: %s", "Internal error", err)
		httputil.JSON(w, http.StatusInternalServerError, nil)
		return
	}
	if s != nil {
		log.Print("Duplicated URL")
		httputil.JSON(w, http.StatusOK, Response{URL: s.URL, ShortenURL: s.ShortenURL})
		return
	}

	shortenURL, err := shortenurl.Create(params.URL)
	if err != nil {
		log.Printf("%s, Warning: %+v", "Internal error", err)
		httputil.JSON(w, http.StatusInternalServerError, nil)
		return
	}

	response := struct {
		URL        string `json:"url"`
		ShortenURL string `json:"shortenUrl"`
	}{
		URL:        params.URL,
		ShortenURL: shortenURL,
	}

	log.Printf("Shorten URL created Successfully. %+v", response)
	httputil.JSON(w, http.StatusCreated, response)
}

// ShortenURL .
func ShortenURL(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	sURL, ok := vars["shortenURL"]
	if sURL == "health" {
		log.Println("Health")
		httputil.JSON(w, http.StatusNoContent, nil)
		return
	}
	valid := validShortenURL.MatchString(sURL)
	if !ok || !valid {
		httputil.Error(w, http.StatusBadRequest, httputil.CodeErrInvalidJSON, "Invalid shorten url")
		return
	}

	s, err := shortenurl.FindShortenURL(sURL)
	if err != nil {
		log.Printf("%s, Error: %s", "Internal error", err)
		httputil.JSON(w, http.StatusInternalServerError, nil)
		return
	}
	if s == nil {
		log.Printf("%s", "Not found")
		httputil.JSON(w, http.StatusNotFound, nil)
		return
	}

	log.Printf("%s redirect to %s.", s.ShortenURL, s.URL)
	http.Redirect(w, r, s.URL, http.StatusTemporaryRedirect)
}
