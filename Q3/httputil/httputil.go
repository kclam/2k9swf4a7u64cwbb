package httputil

import (
	"net/http"

	"github.com/unrolled/render"
)

const (
	// CodeErrInvalidJSON .
	CodeErrInvalidJSON = 10001
	// CodeErrInvalidShortenURL .
	CodeErrInvalidShortenURL = 10002
)

type error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// JSON .
func JSON(rw http.ResponseWriter, status int, v interface{}) {
	r := render.New()
	r.JSON(rw, status, v)
}

// Error .
func Error(rw http.ResponseWriter, status, errCode int, errMessage string) {
	r := render.New()
	r.JSON(rw, status, error{errCode, errMessage})
}
