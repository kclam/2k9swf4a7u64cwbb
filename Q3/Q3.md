## System design

The service is implemented by 1 go program.  It will connect to 1 mongoDB (DocumentDB in AWS) as the data storage. 
The program is deployed to an AWS ECS cluster. 

## Missing components

It may need cache for the mongoDB. Since the data size for a record is small. We can consider to use local in-memory 
cache such as [go-cache](https://github.com/patrickmn/go-cache).  It is simple and faster than external cache such as [redis](https://redis.io/).

For the secret, it can use aws secret manager to handle other than inserted from GitLab.

## Testing

Domain of the load-balancer is `offsite-test-q3-269086302.us-east-2.elb.amazonaws.com`. 
```
KCs-MacBook-Pro:Q3 lam$ cat ~tools/newurl-post.sh 
#!/usr/bin/env bash
#
content="Content-Type: application/json"
data="{\"url\": \"https://www.google.com\"}"
host="offsite-test-q3-269086302.us-east-2.elb.amazonaws.com:80"
curl -is -H "${content}" -X POST -d "${data}" http://${host}/newurl > ~response
cat ~response | grep -v "{\".*\"}"
cat ~response | grep "{\".*\"}" | json_pp
rm ~response
KCs-MacBook-Pro:Q3 lam$ ~tools/newurl-post.sh 
HTTP/1.1 200 OK
Date: Fri, 01 May 2020 11:23:12 GMT
Content-Type: application/json; charset=UTF-8
Content-Length: 57
Connection: keep-alive

{
   "shortenUrl" : "5mlEsLQOY",
   "url" : "https://www.google.com"
}
```

Test the shortened url, http://offsite-test-q3-269086302.us-east-2.elb.amazonaws.com/5mlEsLQOY
```
KCs-MacBook-Pro:Q3 lam$ curl -i http://offsite-test-q3-269086302.us-east-2.elb.amazonaws.com:80/j8Y6RL7su
HTTP/1.1 307 Temporary Redirect
Date: Fri, 01 May 2020 11:23:55 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 58
Connection: keep-alive
Location: https://www.google.com

<a href="https://www.google.com">Temporary Redirect</a>.

KCs-MacBook-Pro:Q3 lam$ 
```

In here, I select to use `307` for the redirect instead of `301` which cannot be changed once the browser received.

## Deployment

The deployment is done by the GitLab-CI file, `.gitlab-ci.yml`.  It will go thought 3 stages: test, build and deploy.

### Stage test

It will pull a golang-alpine image for compile and test.  
Testing result can find in the GitLab pipline. For examples,

https://gitlab.com/kclam/2k9swf4a7u64cwbb/-/jobs/534603973

### Stage build
It will use docker image for docker build the image. If it is success, it will push to project's own registry.

https://gitlab.com/kclam/2k9swf4a7u64cwbb/container_registry

### Stage deploy

It use `GitLab` provided `aws-base` image for deployment.  

It will deploy the code to a cluster called, `offsite-test-q3`, which is defined in the ci-file. 
So it is easy to change to other cluster (e.g. a EC2 type cluster) by updating the ci-file.
