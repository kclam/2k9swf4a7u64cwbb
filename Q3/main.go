package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/kclam/2k9swf4a7u64cwbb/config"
	"gitlab.com/kclam/2k9swf4a7u64cwbb/handler"
	"gitlab.com/kclam/2k9swf4a7u64cwbb/shortenurl"
)

func init() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
	config.Init()
	shortenurl.Init()
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/newurl", handler.NewURL).Methods("POST")
	r.HandleFunc("/{shortenURL}", handler.ShortenURL).Methods("GET")

	log.Println("Server start :80")
	http.ListenAndServe(":80", r)
}
