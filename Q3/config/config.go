package config

import (
	"log"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/spf13/viper"
)

var config Config

// Config ..
type Config struct {
	DB DBConfig
	v  *viper.Viper
}

// DBConfig ..
type DBConfig struct {
	DynamoRegion          string `json:"dynamo_region"`
	DynamoAddress         string `json:"dynamo_address"`
	DynamoTable           string `json:"dynamo_table"`
	DynamoAccessKeyID     string `json:"dynamo_access_key_id"`
	DynamoSecretAccessKey string `json:"dynamo_secret_access_key"`
}

func (cfg *Config) loadFromEnv() error {
	v := cfg.v
	v.AutomaticEnv()
	v.BindEnv("DB.DynamoRegion", "DYNAMO_REGION")
	v.BindEnv("DB.DynamoAddress", "DYNAMO_ADDRESS")
	v.BindEnv("DB.DynamoTable", "DYNAMO_TABLE")
	v.BindEnv("DB.DynamoAccessKeyID", "DYNAMO_ACCESS_KEY_ID")
	v.BindEnv("DB.DynamoSecretAccessKey", "DYNAMO_SECRET_ACCESS_KEY")

	err := v.Unmarshal(&cfg)
	if err != nil {
		return err
	}
	return nil
}

func (cfg Config) validate() error {
	errMsg := "missing in config"
	err := validation.ValidateStruct(&cfg.DB,
		validation.Field(&cfg.DB.DynamoRegion, validation.Required.Error(errMsg)),
		validation.Field(&cfg.DB.DynamoAddress, validation.Required.Error(errMsg)),
		validation.Field(&cfg.DB.DynamoTable, validation.Required.Error(errMsg)),
		validation.Field(&cfg.DB.DynamoAccessKeyID, validation.Required.Error(errMsg)),
		validation.Field(&cfg.DB.DynamoSecretAccessKey, validation.Required.Error(errMsg)),
	)
	if err != nil {
		return err
	}
	if err != nil {
		return err
	}
	return nil
}

func newConfig() Config {
	return Config{
		v: viper.New(),
	}
}

// Load ..
func Load() error {
	config = newConfig()
	if err := config.loadFromEnv(); err != nil {
		return err
	}
	if err := config.validate(); err != nil {
		return err
	}
	return nil
}

// Get get app config
func Get() Config {
	return config
}

// Init .
func Init() {
	err := Load()
	if err != nil {
		log.Fatal(err)
	}
}
