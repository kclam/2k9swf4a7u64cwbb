#!/usr/bin/env bash
#
zcat access.log.gz | grep "1[0-9]/Jun/2019:.*HTTP/[0-9]\.[0-9]" | awk '{count[$1]++} END{ for (i in count) print count[i], i}' | sort -r -n -k 1,1 | head -n 10
