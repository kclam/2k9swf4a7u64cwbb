#!/usr/bin/env bash
#
ips=$(zcat < access.log.gz | grep "HTTP/[0-9]\.[0-9]" | awk '{print $1}' | awk '{count[$1]++} END{ for (i in count) print i,count[i]}')
for ip in $ips
do 
    if [[ $ip == *"."* || $ip == *":"* ]]
    then 
        country=$(geoiplookup $ip)
        if [[ $country == *"can"*"t resolve"* || $country == *"not found"* ]]
        then
            printf "%s" --
        else
            printf $( echo $country | awk '{print $4}' | sed 's/[,]//g' )
        fi
    else 
        printf " $ip\n"
    fi
done | awk '{count[$1] += $2} END{ for (i in count) print i,count[i]}' | sort -n -k 2,2
